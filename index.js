//#3
fetch(`https://jsonplaceholder.typicode.com/todos/`)
.then( data => data.json() )
.then( data => {
    console.log(data)
    })

//#4
// fetch(`https://jsonplaceholder.typicode.com/todos/`)
// .then( data => data.json() )
// .then( data => {
//     let todo=data.map(element =>{
//         return element.title
//     })
//     todo.forEach(title => {       
//         console.log(title)
//     })
// })

//#5
fetch(`https://jsonplaceholder.typicode.com/todos/1`)
.then( data => data.json() )
.then( data => {
    console.log(data)
})

//#6
fetch(`https://jsonplaceholder.typicode.com/todos/1`)
.then(data => data.json())
.then(data => {
    const {title, completed} = data
    console.log(`The item "${title}" on the list has a status of ${completed}`)
})

//#7
fetch(`https://jsonplaceholder.typicode.com/todos`, {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        userId: 1,
        id: 201,
        title: "Created to do list Item",
        completed:false
    })
})
.then( response => response.json() )
.then( response => {
    console.log(response)
})

//#8 
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        dateCompleted:"Pending",
        description:"Updated description",
        status:"pending",
        title:"Update to do list item"
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})

//#9
fetch(`https://jsonplaceholder.typicode.com/todos/2`, {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        completed:false,
        dateCompleted:"07/09/21",
        status:"Complete",
        userId:1
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})

//#10
fetch(`https://jsonplaceholder.typicode.com/todos/3`, {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Corrected Post thru patch method"
    })
})
.then( data => data.json())
.then(data => {
    console.log(data)
})

//#11
fetch(`https://jsonplaceholder.typicode.com/todos/4`, {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        dateCompleted:"03/07/22",
        status:"Complete",
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})

//#12
fetch(`https://jsonplaceholder.typicode.com/posts/1`, {
    method: "DELETE"
})